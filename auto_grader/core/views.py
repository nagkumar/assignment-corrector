from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, HttpResponseBadRequest
from ratelimit.decorators import ratelimit
from django.shortcuts import render

# Create your views here.
from .models import Submission, Assignment
from .tasks import queue_corrector
from .corrector import start


@ratelimit(key='ip', rate='1/m', block=True)
def submit(request):

    email_id = request.POST.get("email_id", None)
    if not email_id:
        return HttpResponseBadRequest("No email id provided")
    sjsu_id = request.POST.get("sjsu_id", None)
    if not sjsu_id:
        return HttpResponseBadRequest("No SJSU ID provided")
    api_url = request.POST.get("api_url", None)
    if not api_url:
        return HttpResponseBadRequest("No API URL provided")
    if api_url.endswith("/"):
        api_url = api_url[:-1]
    submit_for = request.POST.get("submit_for", None)
    if not submit_for:
        return HttpResponseBadRequest("No assignment number provided")
    assignment = None
    try:
        assignment = Assignment.objects.get(name=submit_for)
    except ObjectDoesNotExist:
        return HttpResponseBadRequest("Assignment number incorrect")

    full_name = request.POST.get("full_name", None)
    if not full_name:
        return HttpResponseBadRequest("No full name")
    submission = Submission(
        sjsu_id=sjsu_id,
        name=full_name,
        email=email_id,
        server_url=api_url,
        submit_for=submit_for,
        assignment=assignment
    )
    submission.save()
    if settings.DEBUG:
        start(request)
    else:
        queue_corrector.delay(submission)
    return HttpResponse("We have received your request. It will be processed and an email will be sent to %s " % email_id)