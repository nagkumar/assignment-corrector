from django.conf import settings
from django.db import models

# Create your models here.
from model_utils.models import TimeStampedModel


class Assignment(TimeStampedModel):
    name = models.CharField(max_length=200)
    description = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return self.name


class Question(TimeStampedModel):
    name = models.CharField(max_length=200)
    path = models.CharField(max_length=255, default="/")
    description = models.TextField(null=True, blank=True)
    assignment = models.ForeignKey(Assignment, related_name='questions')
    type = models.CharField(max_length=20, choices=settings.QUESTION_TYPES, default=settings.GET)

    def __unicode__(self):
        return self.name


class TestCase(models.Model):
    name = models.CharField(max_length=200)
    path = models.CharField(max_length=255, default="/")
    question = models.ForeignKey(Question, related_name='test_cases')
    payload = models.TextField(null=True, blank=True)
    expected_status_code = models.CharField(max_length=20)
    expected_body = models.TextField(null=True, blank=True)
    order = models.IntegerField(default=1)

    def __unicode__(self):
        return self.name


class Submission(TimeStampedModel):
    sjsu_id = models.CharField(max_length=20)
    name = models.CharField(max_length=200)
    email = models.EmailField()
    server_url = models.CharField(max_length=200)
    submit_for = models.CharField(max_length=200)
    assignment = models.ForeignKey(Assignment, null=True, blank=True, related_name='submissions')

    def __unicode__(self):
        return "%s %s %s %s %s" (self.sjsu_id, self.name, self.email, self.server_url, self.assignment)


class ResultPerTestCase(TimeStampedModel):
    submission = models.ForeignKey(Submission, related_name='results_per_test_case')
    test_case = models.OneToOneField(TestCase, related_name='results')
    result = models.ForeignKey('ResultPerAssignment', related_name='test_case_results')
    did_pass = models.BooleanField()

    def __unicode__(self):
        return "%s %s" (self.submission, str(self.did_pass))


class ResultPerAssignment(TimeStampedModel):
    submission = models.OneToOneField(Submission, related_name='results')
    score = models.CharField(max_length=20)
    email_sent = models.BooleanField(default=False)

    def __unicode__(self):
        return "%s %s" (self.submission, self.score)