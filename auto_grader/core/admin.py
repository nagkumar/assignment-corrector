from django.contrib import admin
from .models import Assignment, Question, TestCase, Submission, ResultPerTestCase, ResultPerAssignment
# Register your models here.


class QuestionsInline(admin.StackedInline):
    model = Question
    extra = 4


class AssignmentAdmin(admin.ModelAdmin):
    inlines = [QuestionsInline]


class TestCaseInline(admin.StackedInline):
    model = TestCase
    extra = 4


class QuestionAdmin(admin.ModelAdmin):
    list_filter = ("assignment", )
    inlines = [TestCaseInline]


class ResultPerAssignmentAdmin(admin.ModelAdmin):
    list_filter = ("submission", )


class SubmissionAdmin(admin.ModelAdmin):
    list_filter = ("assignment", )

admin.site.register(Assignment, AssignmentAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(TestCase)
admin.site.register(Submission, SubmissionAdmin)
admin.site.register(ResultPerTestCase)
admin.site.register(ResultPerAssignment, ResultPerAssignmentAdmin)