from __future__ import absolute_import
__author__ = 'nagkumar'
from celery import shared_task
from .corrector import start


@shared_task
def queue_corrector(submission_obj):
    print "Task Started"
    return start(submission_obj)