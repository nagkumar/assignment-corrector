# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20151018_2254'),
    ]

    operations = [
        migrations.AddField(
            model_name='testcase',
            name='order',
            field=models.IntegerField(default=1),
        ),
    ]
