# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import model_utils.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Assignment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField(null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField(null=True, blank=True)),
                ('type', models.CharField(default=b'get', max_length=20, choices=[(b'get', b'get'), (b'post', b'get'), (b'put', b'get'), (b'delete', b'get'), (b'options', b'get'), (b'head', b'get'), (b'trace', b'get'), (b'connect', b'get')])),
                ('assignment', models.ForeignKey(related_name='questions', to='core.Assignment')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ResultPerAssignment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('score', models.CharField(max_length=20)),
                ('email_sent', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ResultPerTestCase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('did_pass', models.BooleanField()),
                ('result', models.ForeignKey(related_name='test_case_results', to='core.ResultPerAssignment')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Submission',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('sjsu_id', models.CharField(max_length=20)),
                ('name', models.CharField(max_length=200)),
                ('email', models.EmailField(max_length=254)),
                ('server_url', models.CharField(max_length=200)),
                ('submit_for', models.CharField(max_length=200)),
                ('assignment', models.ForeignKey(related_name='submissions', blank=True, to='core.Assignment', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='TestCase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('payload', models.TextField(null=True)),
                ('expected_status_code', models.CharField(max_length=20)),
                ('expected_body', models.TextField(null=True)),
                ('question', models.ForeignKey(related_name='test_cases', to='core.Question')),
            ],
        ),
        migrations.AddField(
            model_name='resultpertestcase',
            name='submission',
            field=models.ForeignKey(related_name='results_per_test_case', to='core.Submission'),
        ),
        migrations.AddField(
            model_name='resultpertestcase',
            name='test_case',
            field=models.OneToOneField(related_name='results', to='core.TestCase'),
        ),
        migrations.AddField(
            model_name='resultperassignment',
            name='submission',
            field=models.OneToOneField(related_name='results', to='core.Submission'),
        ),
    ]
