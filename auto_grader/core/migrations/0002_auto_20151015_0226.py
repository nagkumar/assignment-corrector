# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='path',
            field=models.CharField(default=b'/', max_length=255),
        ),
        migrations.AlterField(
            model_name='question',
            name='type',
            field=models.CharField(default=b'get', max_length=20, choices=[(b'get', b'get'), (b'post', b'post'), (b'put', b'put'), (b'delete', b'delete'), (b'options', b'options'), (b'head', b'head'), (b'trace', b'trace'), (b'connect', b'connect')]),
        ),
    ]
