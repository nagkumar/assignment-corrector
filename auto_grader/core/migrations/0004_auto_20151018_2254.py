# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_testcase_path'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testcase',
            name='expected_body',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='testcase',
            name='payload',
            field=models.TextField(null=True, blank=True),
        ),
    ]
