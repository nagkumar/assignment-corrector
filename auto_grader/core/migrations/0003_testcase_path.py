# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20151015_0226'),
    ]

    operations = [
        migrations.AddField(
            model_name='testcase',
            name='path',
            field=models.CharField(default=b'/', max_length=255),
        ),
    ]
