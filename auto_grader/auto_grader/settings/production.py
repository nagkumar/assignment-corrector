__author__ = 'nagkumar'
from .base import *

DEBUG = False
SECRET_KEY = 'p8&15yj4_%+uk7&6rj%d8%prn18vbji%9jp-udt)6+j_(ec_$$'
ALLOWED_HOSTS = ["autograder.nagkumar.com"]
# STATICFILES_DIRS = (
#     os.path.join(BASE_DIR, "static"),
#     '/home/django/assignment-corrector/auto_grader/static',
# )
STATIC_ROOT = '/home/django/assignment-corrector/auto_grader/static'
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'auto_grader',
        'USER': 'django',
        'PASSWORD': 'django',
        'HOST': '',
        'PORT': '',
    }
}