__author__ = 'nagkumar'
from fabric.colors import green, cyan, magenta
from fabric.context_managers import cd, shell_env
from fabric.operations import run, sudo, local
from fabric.state import env

env.hosts = ["autograder.nagkumar.com"]
env.user = 'django'

dir_in_server = '/home/django/assignment-corrector/auto_grader'


def deploy():
    print(cyan("Enter your git commit message"))
    msg = raw_input()
    local('git add .')
    local('git commit -am "%s"' % msg)
    print(green("Listing Branches"))
    local('git branch -a')
    print(cyan("Enter a branch name to push:"))
    branch = raw_input()
    local('git push origin %s' % branch)
    print(green("Deployment complete"))
    with cd(dir_in_server):
        print(magenta("Inside server"))
        run("git reset --hard || true")
        run("git pull")
        run("source /home/django/dj/bin/activate && pip install -r ../requirements.txt")
        with shell_env(DJANGO_SETTINGS_MODULE='auto_grader.settings.production'):
            run("source /home/django/dj/bin/activate && ./manage.py collectstatic --noinput")
            run("source /home/django/dj/bin/activate && ./manage.py migrate --no-initial-data")
    sudo("service apache2 restart")
    print(green("Deployment complete"))